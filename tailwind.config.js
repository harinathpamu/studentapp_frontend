module.exports = {
  mode: "jit",
  purge: ["./src/**/*.{js,jsx,ts,tsx}", "./public/index.html"],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        primary: "#00C1B1",
        "primary-light": "#5ff4e3",
        "primary-dark": "#009082",
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
