import api from "../../api";
import Info from "../../components/Info";
import { IStudent, IAddEditStudent, MODE } from "../../types";

import moment from "moment";
import { AxiosError } from "axios";
import { Field, Form, Formik } from "formik";
import { useMutation, useQuery } from "react-query";
import { useParams, useHistory, Link } from "react-router-dom";
import PageLayout from "../../components/Layout";
import * as Yup from "yup";

const StudentValidationSchema = Yup.object().shape({
  firstname: Yup.string().trim().required("firstname is required"),
  lastname: Yup.string().trim().required("lastname is required"),
  course: Yup.string().trim().required("course is required"),
  hours: Yup.number().min(1).required("hours is required"),
  price: Yup.number().min(1).required("price is required"),
  dateOfBirth: Yup.string().trim().required("dateOfBirth is required"),
});

function AddEditStudent(props: IAddEditStudent) {
  const { mode } = props;
  const history = useHistory();
  const inEditMode = mode === MODE.EDIT;
  const params = useParams<{ id: string }>();

  const { data, status, error } = useQuery<IStudent, AxiosError>(
    ["student", params.id],
    () => {
      return api
        .get<IStudent>(`/student/${params.id}`)
        .then((response) => response.data);
    },
    {
      enabled: inEditMode && params.id ? true : false,
    }
  );

  const addMutation = useMutation((newStudent: IStudent) => {
    return api.post("/student", newStudent);
  });

  const updateMutation = useMutation((updateStudent: IStudent) => {
    return api.patch(`/student/${params.id}`, updateStudent);
  });

  if (inEditMode && status === "loading") {
    return <Info message="Loading..." />;
  }

  if (inEditMode && status === "error") {
    if (error?.response?.status === 404) {
      return <Info message="Record not found :(" />;
    } else if (error?.response?.status === 400) {
      return <Info message="Bad request" />;
    } else {
      return <Info message="Something went wrong..." />;
    }
  }

  return (
    <Formik
      validationSchema={StudentValidationSchema}
      initialValues={{
        firstname: "",
        lastname: "",
        course: "",
        hours: 0,
        price: 0,
        ...data,
        dateOfBirth: moment(data?.dateOfBirth).format("YYYY-MM-DD") || "",
      }}
      onSubmit={(values: IStudent) => {
        if (!inEditMode) {
          addMutation.mutate(values, {
            onSuccess: () => {
              history.push("/students");
            },
          });
        }
        if (inEditMode) {
          updateMutation.mutate(values, {
            onSuccess: () => {
              history.push("/students");
            },
          });
        }
      }}
    >
      {({ dirty }) => {
        return (
          <PageLayout>
            <Form className="bg-white rounded-md overflow-hidden space-y-3 shadow mt-20">
              <div className="px-5 py-4 space-y-3">
                <h1 className="text-gray-900 text-lg">
                  {inEditMode ? "Edit" : "Add"} Student
                </h1>
                <div className="grid grid-flow-row lg:grid-cols-5 lg:grid-flow-col auto-cols-auto  w-full gap-3">
                  <div className="flex flex-col">
                    <label
                      htmlFor="firstname"
                      className="text-gray-700 text-sm capitalize"
                    >
                      First Name
                    </label>
                    <Field
                      id="firstname"
                      required
                      name="firstname"
                      className="form-input max-w-min min-w-full"
                    />
                  </div>
                  <div className="flex flex-col">
                    <label
                      htmlFor="lastname"
                      className="text-gray-700 text-sm capitalize"
                    >
                      Last Name
                    </label>
                    <Field
                      required
                      name="lastname"
                      id="lastname"
                      className="form-input max-w-min min-w-full"
                    />
                  </div>
                  <div className="flex flex-col">
                    <label
                      htmlFor="dateOfBirth"
                      className="text-gray-700 text-sm capitalize"
                    >
                      Date of Birth
                    </label>
                    <Field
                      required
                      className="form-input max-w-min min-w-full"
                      type="date"
                      name="dateOfBirth"
                      id="dateOfBirth"
                    />
                  </div>
                  <div className="flex flex-col lg:col-span-2">
                    <label
                      htmlFor="course"
                      className="text-gray-700 text-sm capitalize"
                    >
                      Course Name
                    </label>
                    <Field
                      required
                      id="course"
                      name="course"
                      className="form-input max-w-min min-w-full"
                    />
                  </div>
                  <div className="flex flex-col">
                    <label
                      htmlFor="hours"
                      className="text-gray-700 text-sm capitalize"
                    >
                      Hours
                    </label>
                    <Field
                      required
                      name="hours"
                      type="number"
                      min="1"
                      id="hours"
                      className="form-input max-w-[100px] min-w-full  text-right "
                    />
                  </div>
                  <div className="flex flex-col">
                    <label
                      htmlFor="price"
                      className="text-gray-700 text-sm capitalize"
                    >
                      Price €
                    </label>
                    <Field
                      id="price"
                      required
                      name="price"
                      type="number"
                      min="1"
                      step="any"
                      className="form-input max-w-[150px] min-w-full text-right"
                    />
                  </div>
                </div>
              </div>
              <div className="py-2 px-4 bg-[#16295A] gap-x-2 flex items-center justify-end">
                <Link to="/students" className="btn btn-secondary inline-block">
                  Cancel
                </Link>
                {!inEditMode && (
                  <button
                    disabled={addMutation.isLoading}
                    type="submit"
                    className="btn btn-primary"
                  >
                    {addMutation.isLoading ? "Adding student..." : "Save"}
                  </button>
                )}
                {inEditMode && (
                  <button
                    disabled={updateMutation.isLoading}
                    type="submit"
                    className="btn btn-primary"
                  >
                    {updateMutation.isLoading
                      ? "Updating student..."
                      : dirty
                      ? "Save changes"
                      : "Save"}
                  </button>
                )}
              </div>
            </Form>
          </PageLayout>
        );
      }}
    </Formik>
  );
}

export default AddEditStudent;
