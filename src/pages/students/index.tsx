import api from "../../api";
import Info from "../../components/Info";
import { IIStudent, IStudentsTable } from "../../types";

import moment from "moment";
import { AxiosError } from "axios";
import { Link } from "react-router-dom";
import { useMutation, useQuery, useQueryClient } from "react-query";
import PageLayout from "../../components/Layout";

function Students() {
  const { data, status, error } = useQuery<IIStudent[], AxiosError>(
    "students",
    () => {
      return api
        .get<IIStudent[]>("/students")
        .then((response) => response.data);
    }
  );

  if (status === "loading") {
    return <Info message="Loading..." />;
  }

  if (status === "error") {
    if (error?.response?.status === 404) {
      return <Info message="Record not found :(" />;
    } else if (error?.response?.status === 400) {
      return <Info message="Bad request" />;
    } else {
      return <Info message="Something went wrong..." />;
    }
  }

  return (
    <PageLayout>
      <div className="space-y-3">
        <div className="text-right mt-20">
          <Link to="/student/add" className="btn btn-primary inline-block">
            Add Student
          </Link>
        </div>
        {data && <StudentsTable students={data} />}
      </div>
    </PageLayout>
  );
}

export default Students;

function StudentsTable(props: IStudentsTable) {
  const { students = [] } = props;
  const queryClient = useQueryClient();

  const deleteMutation = useMutation((deleteStudentId: number) => {
    return api.delete(`/student/${deleteStudentId}`);
  });

  if (students?.length === 0) {
    return (
      <div className="h-64 flex items-center justify-center">
        <span>No students yet</span>
      </div>
    );
  }

  return (
    <div className="shadow overflow-auto rounded-md">
      <table>
        <thead>
          <tr>
            <th>first name</th>
            <th>last name</th>
            <th>date of birth</th>
            <th>course</th>
            <th>hours</th>
            <th>price</th>
            <th className="relative px-6 py-3">
              <span className="sr-only">Edit</span>
            </th>
            <th className="relative px-6 py-3">
              <span className="sr-only">Delete</span>
            </th>
          </tr>
        </thead>
        <tbody>
          {students.map((student, i) => (
            <tr key={student.id}>
              <td>{student?.firstname || "NA"}</td>
              <td>{student?.lastname || "NA"}</td>
              <td>
                {student?.dateOfBirth
                  ? moment(student?.dateOfBirth).format("DD/MM/YYYY")
                  : "NA"}
              </td>
              <td>{student?.course || "NA"}</td>
              <td>{student?.hours ? `${student?.hours} H` : "NA"}</td>
              <td>{student?.price ? `${student?.price} €` : "NA"}</td>
              <td className="text-right font-normal">
                <Link
                  to={`/student/edit/${+student.id}`}
                  className="text-primary hover:text-primary-dark"
                >
                  Edit
                </Link>
              </td>
              <td className="text-right font-normal">
                <span
                  role="button"
                  className="text-primary hover:text-primary-dark"
                  onClick={() => {
                    if (!deleteMutation.isLoading) {
                      deleteMutation.mutate(student.id, {
                        onSuccess: () => {
                          queryClient.invalidateQueries("students");
                        },
                      });
                    }
                  }}
                >
                  {deleteMutation.isLoading ? "Deleting..." : "Delete"}
                </span>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}
