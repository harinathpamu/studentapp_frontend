export interface IStudent {
  firstname: string;
  lastname: string;
  dateOfBirth: string;
  course: string;
  hours: number;
  price: number;
}

export interface IIStudent extends IStudent {
  id: number;
}

export interface IAddEditStudent {
  mode: MODE;
}

export interface IStudentsTable {
  students: IIStudent[];
}

export enum MODE {
  ADD,
  EDIT,
}
