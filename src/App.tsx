import { Suspense, lazy } from "react";

import { MODE } from "./types";
import { QueryClient, QueryClientProvider } from "react-query";
import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom";

// Code Splitting
const StudentPage = lazy(() => import("./pages/student"));
const StudentsPage = lazy(() => import("./pages/students"));

function App() {
  const queryClient = new QueryClient();
  return (
    <>
      <QueryClientProvider client={queryClient}>
        <BrowserRouter>
          <Suspense fallback={<div>Please wait ...</div>}>
            <Switch>
              <Route exact path="/students">
                <StudentsPage />
              </Route>
              <Route path="/student/add">
                <StudentPage mode={MODE.ADD} />
              </Route>
              <Route path="/student/edit/:id">
                <StudentPage mode={MODE.EDIT} />
              </Route>
              <Redirect to="/students" />
            </Switch>
          </Suspense>
        </BrowserRouter>
      </QueryClientProvider>
    </>
  );
}

export default App;
