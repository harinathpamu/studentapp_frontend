interface IInfo {
  message: string;
}

function Info(props: IInfo) {
  const { message } = props;
  return (
    <div className="h-screen flex items-center justify-center">
      <span>{message}</span>
    </div>
  );
}

export default Info;
