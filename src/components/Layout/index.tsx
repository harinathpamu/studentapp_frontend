import { ReactNode } from "react";

interface ILayout {
  children: ReactNode;
}

function Layout(props: ILayout) {
  return <div className="md:container md:mx-auto p-2">{props.children}</div>;
}

export default Layout;
