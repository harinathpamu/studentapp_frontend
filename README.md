## Live Demo

Open [https://student-app-ostrom.netlify.app/students](https://student-app-ostrom.netlify.app/students)

## Available Scripts

In the project directory, to run:

### `npm install`

Installs node_modules ( project dependencies )

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm cypress:open`

Launches the cypress test runner in the interactive watch mode.\
See the section about [running tests](https://docs.cypress.io/) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Docker

Install [docker](https://www.docker.com/)

After installing to build the docker image ,in the project directory

To build the docker image

### `docker build --pull --rm -f "Dockerfile" -t student_app_frontend:latest "."`

To run the docker image

### `docker run -d -p 3000:5000 student_app_frontend`

Visit [studentapp](http:localhost:3000/students)
**Note: Student backend required[backend](https://gitlab.com/harinathpamu/studentapp_backend)**

## Libraries used

## ReactJS

A JavaScript library for building user interfaces

- Declarative
- Intuitive
- Adoptable
- Component-Based

See the section about [react](https://reactjs.org/) for more information.

## axios

Promise based HTTP client for the browser and node.js

- Make XMLHttpRequests from the browser
- Make http requests from node.js
- Supports the Promise API
- Intercept request and response
- Transform request and response data
- Cancel requests
- Automatic transforms for JSON data
- Client side support for protecting against XSRF

See the section about [axios](https://github.com/axios/axios) for more information.

## Formik

Build forms in React, without the tears

- Declarative
- Intuitive
- Adoptable

See the section about [formik](https://formik.org/) for more information.

## React Query

Performant and powerful data synchronization for React

- Fetch, cache and update data in your React and React Native applications all without touching any "global state".

See the section about [react-query](https://react-query.tanstack.com/) for more information.

## React Router Dom

- Declarative routing for React apps.

See the section about [react-router-dom](https://reactrouter.com/web/example/basic) for more information.

## TailwindCSS

Rapidly build modern websites without ever leaving your HTML

- A utility-first CSS framework packed with classes like flex, pt-4, text-center and rotate-90 that can be composed to build any design, directly in your markup.

See the section about [tailwindcss](https://tailwindcss.com/) for more information.

## Yup

- Yup is a JavaScript schema builder for value parsing and validation. Define a schema, transform a value to match, validate the shape of an existing value, or both. Yup schema are extremely expressive and allow modeling complex, interdependent validations, or value transformations.

See the section about [Yup](https://github.com/jquense/yup) for more information.
