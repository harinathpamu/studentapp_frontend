/// <reference types="cypress" />

it("Visits the Students Page", () => {
  cy.visit("http://localhost:3001/students");
  cy.url().should("include", "/students");
});

it("Visits the Student Add Page", () => {
  cy.visit("http://localhost:3001/students");
  cy.contains(/Add Student/i).click();
  cy.url().should("include", "/student/add");
});

it("Visiting Students Page From Student Add Page", () => {
  cy.visit("http://localhost:3001/students");
  cy.contains(/Add Student/i).click();
  cy.url().should("include", "/student/add");
  cy.contains(/Cancel/i).click();
  cy.url().should("include", "/students");
});

it("Can Add Student", () => {
  cy.visit("http://localhost:3001/student/add");
  cy.get("#firstname").clear().type("Student name");
  cy.get("#lastname").clear().type("Student lastname");
  cy.get("#dateOfBirth").clear().type("2021-10-12");
  cy.get("#course").clear().type("ReactJS");
  cy.get("#hours").clear().type("10");
  cy.get("#price").clear().type("120");
  cy.contains(/Save/i).click();
  cy.url().should("include", "/students");
  cy.get("tbody tr")
    .last()
    .should(
      "have.text",
      "Student nameStudent lastname12/10/2021ReactJS10 H120 €EditDelete"
    );
});

it("Can Edit Student", () => {
  cy.get("tbody tr").first().contains(/Edit/i).click({ force: true });
  cy.url().should("include", "/student/edit");
  cy.get("#firstname").clear().type("Student name updated");
  cy.contains(/Save changes/i).click();
  cy.url().should("include", "/students");
  cy.contains(/Student name updated/);
});

it("Can Delete Student", () => {
  cy.visit("http://localhost:3001/students");
  cy.get("tbody tr")
    .last()
    .contains(/Delete/i)
    .click({ force: true });
  cy.get("Student name").should("not.exist");
});
