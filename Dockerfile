FROM node:12-alpine
WORKDIR /app
COPY src .
RUN npm install
RUN npm run build
EXPOSE 5000
CMD ["npm", "run","serve"]